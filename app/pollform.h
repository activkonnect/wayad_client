#ifndef POLLFORM_H
#define POLLFORM_H

#include <QWidget>
#include <QShortcut>
#include <QDateTime>
#include <QStandardItemModel>

#include "referencesmodel.h"

namespace Ui {
class PollForm;
}

class PollForm : public QWidget
{
    Q_OBJECT

public:
    explicit PollForm(QStandardItemModel *references, QWidget *parent = 0);
    ~PollForm();

    virtual bool eventFilter(QObject *object, QEvent *event);

signals:
    void logEnteredMessage(QString message, const QDateTime &openDate);
    void logEnteredReference(int reference, const QDateTime &openDate);
    void filterEntered(QString filter);
    void needAnother();

private slots:
    void on_btnNothing_clicked();
    void on_btnAdd_clicked();
    void on_btnAnother_clicked();

    void doNothing();
    void doAdd();
    void doAnother();

    void center();
    void activated(int index);

private:
    Ui::PollForm *ui;
    QShortcut *shortcutNothing, *shortcutAdd, *shortcutAnother;
    QDateTime openDate;
    QStandardItemModel *references;
    int selectedId;

    virtual void closeEvent(QCloseEvent *);
};

#endif // POLLFORM_H
