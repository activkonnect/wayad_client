#include "singleapp.h"

#include <QApplication>
#include <QSharedMemory>
#include <QtDebug>
#include <QUdpSocket>

SingleApp::SingleApp(QString appKey, QObject *parent) :
    QObject(parent),
    appKey(appKey)
{
    connect(&socket, SIGNAL(readyRead()), this, SLOT(gotDatagram()));
}

SingleApp::~SingleApp()
{
}

bool SingleApp::isOpen()
{
    if (socket.bind(QHostAddress::LocalHost, KEY_PORT)) {
        return false;
    } else {
        socket.writeDatagram(QByteArray(), QHostAddress::LocalHost, KEY_PORT);
        return true;
    }
}

QString SingleApp::memoryKey()
{
    return appKey;
}

void SingleApp::gotDatagram()
{
    while (socket.hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(socket.pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;

        socket.readDatagram(datagram.data(), datagram.size(), &sender, &senderPort);
    }

    emit anotherStart();
}

