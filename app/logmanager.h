#ifndef LOGMANAGER_H
#define LOGMANAGER_H

#include <QObject>
#include <QSettings>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QUrlQuery>
#include <QUrl>
#include <QStandardItemModel>
#include <QMap>

#include "referencesmodel.h"

class LogManager : public QObject
{
    Q_OBJECT
public:
    explicit LogManager(QObject *parent = 0);
    ~LogManager();

    QString getEndpoint();
    QString getToken();
    QStandardItemModel *getReferences();

public slots:
    void setEndpoint(const QString &endpoint);
    void setToken(const QString &token);
    void reloadReferences();
    void apiCreateEntryMessage(const QString &message, const QDateTime &openDate);
    void apiCreateEntryReference(int reference, const QDateTime &openDate);

private:
    QNetworkRequest apiRequest(const QString &path, const QUrlQuery &query = QUrlQuery());
    QNetworkRequest apiRequest(const QUrl &url, const QUrlQuery &query = QUrlQuery());
    void apiPostRequest(QNetworkRequest request, const QByteArray &data);
    void apiCreateEntry(const QJsonObject &data, const QDateTime &openDate);
    QString makeHyperlink(QString resource, int id);

    static const QString SETTINGS_API_ENDPOINT;
    static const QString SETTINGS_API_TOKEN;

    QNetworkAccessManager qnam;
    QSettings settings;
    QString endpoint;
    QString token;
    QStandardItemModel references;
    QMap<int, QString> referencesById;
};

#endif // LOGMANAGER_H
