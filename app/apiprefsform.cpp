#include "apiprefsform.h"
#include "ui_apiprefsform.h"

ApiPrefsForm::ApiPrefsForm(LogManager *manager, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ApiPrefsForm),
    manager(manager)
{
    ui->setupUi(this);
}

ApiPrefsForm::~ApiPrefsForm()
{
    delete ui;
}

void ApiPrefsForm::showEvent(QShowEvent *)
{
    ui->endpoint->setText(manager->getEndpoint());
    ui->token->setText(manager->getToken());
}

void ApiPrefsForm::on_buttonBox_accepted()
{
    emit gotEndpoint(ui->endpoint->text());
    emit gotToken(ui->token->text());
    close();
}

void ApiPrefsForm::on_buttonBox_rejected()
{
    close();
}

void ApiPrefsForm::on_token_returnPressed()
{
    on_buttonBox_accepted();
}

void ApiPrefsForm::on_endpoint_returnPressed()
{
    on_buttonBox_accepted();
}
