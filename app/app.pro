#-------------------------------------------------
#
# Project created by QtCreator 2015-01-08T21:02:31
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = app
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    pollform.cpp \
    singleapp.cpp \
    logmanager.cpp \
    apiprefsform.cpp \
    referencesmodel.cpp

HEADERS  += mainwindow.h \
    pollform.h \
    singleapp.h \
    logmanager.h \
    apiprefsform.h \
    referencesmodel.h

FORMS    += mainwindow.ui \
    pollform.ui \
    apiprefsform.ui

RESOURCES += \
    resources.qrc
