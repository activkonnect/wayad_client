#include "referencesmodel.h"

#include <QJsonValue>
#include <QJsonObject>

ReferencesModel::ReferencesModel(QObject *parent) : QAbstractListModel(parent)
{

}

ReferencesModel::~ReferencesModel()
{

}

int ReferencesModel::rowCount(const QModelIndex &parent) const
{
    if (parent.parent() == parent) {
        return messages.length();
    } else {
        return 0;
    }
}

QVariant ReferencesModel::data(const QModelIndex &index, int role) const
{
    if (index.column() > 0 || index.row() >= rowCount(index.parent())) {
        return QVariant();
    }

    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        return messages[index.row()];
    } else if (role == Qt::UserRole) {
        return ids[index.row()];
    }

    return QVariant();
}

QVariant ReferencesModel::headerData(int, Qt::Orientation, int) const
{
    return QVariant();
}

void ReferencesModel::update(QJsonArray data)
{
    beginResetModel();

    ids.clear();
    messages.clear();

    foreach (QJsonValue rawRow, data) {
        QJsonObject row = rawRow.toObject();
        ids.append(row["id"].toInt());
        messages.append(row["key"].toString() + " – " + row["message"].toString());
    }

    endResetModel();
}

