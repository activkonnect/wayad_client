#include "logmanager.h"

#include <QtDebug>
#include <QNetworkAccessManager>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonDocument>
#include <QNetworkReply>

const QString LogManager::SETTINGS_API_ENDPOINT = QString("api/endpoint");
const QString LogManager::SETTINGS_API_TOKEN = QString("api/token");

LogManager::LogManager(QObject *parent) :
    QObject(parent),
    endpoint(settings.value(SETTINGS_API_ENDPOINT, "").toString()),
    token(settings.value(SETTINGS_API_TOKEN, "").toString())
{
}

LogManager::~LogManager()
{

}

QString LogManager::getEndpoint()
{
    return endpoint;
}

QString LogManager::getToken()
{
    return token;
}

QStandardItemModel *LogManager::getReferences()
{
    return &references;
}

void LogManager::setEndpoint(const QString &endpoint)
{
    if (!endpoint.endsWith("/")) {
        this->endpoint = endpoint + "/";
    } else {
        this->endpoint = endpoint;
    }

    settings.setValue(SETTINGS_API_ENDPOINT, this->endpoint);
}

void LogManager::setToken(const QString &token)
{
    settings.setValue(SETTINGS_API_TOKEN, token);
    this->token = token;
}

void LogManager::reloadReferences()
{
    QNetworkRequest request = apiRequest("references/", QUrlQuery());
    QNetworkAccessManager *am = new QNetworkAccessManager();

    for (int i = 0; i < references.rowCount(); i += 1) {
        delete references.item(i);
    }

    references.clear();
    references.appendRow(new QStandardItem());

    auto handler = [=](QNetworkReply *reply) {
        if (reply->error() == QNetworkReply::NoError) {
            QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());

            if (!doc.isObject()) {
                return;
                am->deleteLater();
            }

            if (doc.object()["results"].isArray()) {
                foreach (QJsonValue rawRow, doc.object()["results"].toArray()) {
                    QJsonObject row = rawRow.toObject();
                    QStandardItem *item;

                    if (!row["key"].toString().isEmpty()) {
                        item = new QStandardItem(row["key"].toString() + " – " + row["message"].toString());
                    } else {
                        item = new QStandardItem(row["message"].toString());
                    }

                    item->setData(row["id"].toInt());

                    references.appendRow(item);
                    referencesById[row["id"].toInt()] = item->text();
                }
            }

            if (doc.object()["next"].isString()) {
                QUrl url(doc.object()["next"].toString());
                am->get(apiRequest(url, QUrlQuery(url)));
            } else {
                am->deleteLater();
            }
        }
    };

    connect(am, &QNetworkAccessManager::finished, handler);

    am->get(request);
}

QNetworkRequest LogManager::apiRequest(const QString &path, const QUrlQuery &query)
{
    QUrl url(endpoint + path);
    return apiRequest(url, query);
}

QNetworkRequest LogManager::apiRequest(const QUrl &url, const QUrlQuery &query)
{
    QUrl workUrl(url);
    workUrl.setQuery(query);
    QNetworkRequest request(workUrl);

    request.setRawHeader("Authorization", ("Token " + token).toUtf8());
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    return request;
}

void LogManager::apiPostRequest(QNetworkRequest request, const QByteArray &data)
{
    qnam.post(request, data);
}

void LogManager::apiCreateEntryMessage(const QString &message, const QDateTime &openDate)
{
    QJsonObject data;
    data["message"] = message;
    apiCreateEntry(data, openDate);
}

void LogManager::apiCreateEntryReference(int reference, const QDateTime &openDate)
{
    QJsonObject data;
    data["reference"] = makeHyperlink("references", reference);
    data["message"] = referencesById[reference];
    apiCreateEntry(data, openDate);
}

void LogManager::apiCreateEntry(const QJsonObject &data, const QDateTime &openDate)
{
    QNetworkRequest request = apiRequest("entries/");
    QJsonObject newData(data);
    newData["begin"] = openDate.toString(Qt::ISODate);

    apiPostRequest(request, QJsonDocument(newData).toJson());
}

QString LogManager::makeHyperlink(QString resource, int id)
{
    return endpoint + resource + "/" + QString::number(id) + "/";
}
