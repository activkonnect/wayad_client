#ifndef APIPREFSFORM_H
#define APIPREFSFORM_H

#include <QWidget>

#include "logmanager.h"

namespace Ui {
class ApiPrefsForm;
}

class ApiPrefsForm : public QWidget
{
    Q_OBJECT

public:
    explicit ApiPrefsForm(LogManager *manager, QWidget *parent = 0);
    ~ApiPrefsForm();

signals:
    void gotEndpoint(QString endpoint);
    void gotToken(QString token);

protected:
    virtual void showEvent(QShowEvent *);

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

    void on_token_returnPressed();

    void on_endpoint_returnPressed();

private:
    Ui::ApiPrefsForm *ui;
    LogManager *manager;
};

#endif // APIPREFSFORM_H
