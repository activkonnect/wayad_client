#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QTimer>

#include "logmanager.h"
#include "pollform.h"
#include "apiprefsform.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void iconClicked(QSystemTrayIcon::ActivationReason);
    void toggle();
    void getBack();
    void updateColor();
    void updateTime();
    void askLog(bool force = true);
    void askLogSoft();
    void reschedule();
    void cleanupForm();

private slots:
    void on_actionMinimize_triggered();
    void on_actionQuit_triggered();
    void on_actionLogSomething_triggered();

    void on_actionConfigureAPI_triggered();

private:
    Ui::MainWindow *ui;

    QTimer *timerIcon;
    QTimer *timerTime;
    QIcon iconGreen, iconRed;
    QSystemTrayIcon *icon;
    PollForm *form;

    LogManager logManager;
    ApiPrefsForm *apiPrefsForm;

    int cycleLength;
    int pauseLenght;

    void setupTrayIcon();
    virtual void closeEvent(QCloseEvent *event);

    double timeToPause();
    double timeToAsk();
};

#endif // MAINWINDOW_H
