#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "pollform.h"

#include <QtDebug>
#include <QCloseEvent>
#include <QApplication>
#include <QDateTime>
#include <QTime>

#include <cmath>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    timerIcon(new QTimer(this)),
    timerTime(new QTimer(this)),
    iconGreen(QIcon(":/img/img/logo-green.svg")),
    iconRed(QIcon(":/img/img/logo-red.svg")),
    icon(new QSystemTrayIcon(QIcon(":/img/img/logo.svg"), this)),
    form(NULL),
    apiPrefsForm(new ApiPrefsForm(&logManager)),
    cycleLength(1800),
    pauseLenght(300)
{
    ui->setupUi(this);
    setupTrayIcon();

    timerIcon->setInterval(1000.0 / 30.0);
    timerIcon->start();

    timerTime->setInterval(1000);
    timerTime->start();

    connect(icon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(iconClicked(QSystemTrayIcon::ActivationReason)));
    connect(timerIcon, SIGNAL(timeout()), this, SLOT(updateColor()));
    connect(timerTime, SIGNAL(timeout()), this, SLOT(updateTime()));

    connect(apiPrefsForm, SIGNAL(gotEndpoint(QString)), &logManager, SLOT(setEndpoint(QString)));
    connect(apiPrefsForm, SIGNAL(gotToken(QString)), &logManager, SLOT(setToken(QString)));

    reschedule();
    updateTime();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete apiPrefsForm;
}

void MainWindow::iconClicked(QSystemTrayIcon::ActivationReason)
{
    toggle();
}

void MainWindow::toggle()
{
    setVisible(!isVisible());

    if (isVisible()) {
        QApplication::setActiveWindow(this);
    }
}

void MainWindow::getBack()
{
    setVisible(true);
    QApplication::setActiveWindow(this);
}

void MainWindow::updateColor()
{
    static bool lastState = false, initialized = false;
    bool state;
    double animationTime = 10.0;
    double ttp = timeToPause();
    double t = ttp * 400;

    if (0 < ttp && ttp < animationTime) {
        int k = 0.5 * (sqrt(4 * t + 1) - 1);

        if (k % 2) {
            state = true;
        } else {
            state = false;
        }
    } else if (t > 0) {
        state = true;
    } else {
        state = false;
    }

    if (!initialized || lastState != state) {
        if (state) {
            icon->setIcon(iconRed);
        } else {
            icon->setIcon(iconGreen);
        }

        lastState = state;
        initialized = true;
    }
}

void MainWindow::updateTime()
{
    QString time = QTime::currentTime().toString("hh:mm:ss");
    ui->time->setText(time);

    if (timeToPause() > 0) {
        ui->status->setText(tr("Working"));
    } else {
        ui->status->setText(tr("Syncing"));
    }
}

void MainWindow::askLog(bool force)
{
    if (form == NULL || force) {
        logManager.reloadReferences();

        form = new PollForm(logManager.getReferences());
        form->show();

        connect(form, SIGNAL(logEnteredMessage(QString,QDateTime)), &logManager, SLOT(apiCreateEntryMessage(QString, const QDateTime&)));
        connect(form, SIGNAL(logEnteredReference(int, const QDateTime&)), &logManager, SLOT(apiCreateEntryReference(int, const QDateTime&)));
        connect(form, SIGNAL(needAnother()), this, SLOT(askLog()));
        connect(form, SIGNAL(destroyed()), this, SLOT(reschedule()));
        connect(form, SIGNAL(destroyed()), this, SLOT(cleanupForm()));
    }
}

void MainWindow::askLogSoft()
{
    askLog(false);
}

void MainWindow::reschedule()
{
    QTimer::singleShot(timeToAsk() * 1000.0, this, SLOT(askLogSoft()));
}

void MainWindow::cleanupForm()
{
    form = 0;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    setVisible(false);
    event->ignore();
}

double MainWindow::timeToPause()
{
    int currentCycle = QDateTime::currentDateTime().toMSecsSinceEpoch() % (cycleLength * 1000);

    double dCycleLength = (double) cycleLength * 1000.0,
            dCurrentCycle = (double) currentCycle,
            dPauseLength = (double) pauseLenght * 1000.0;

    return (dCycleLength - dCurrentCycle - dPauseLength) / 1000.0;
}

double MainWindow::timeToAsk()
{
    double time = timeToPause();

    if (time < 0) {
        return time + cycleLength;
    } else {
        return time;
    }
}

void MainWindow::on_actionMinimize_triggered()
{
    toggle();
}

void MainWindow::on_actionQuit_triggered()
{
    QApplication::quit();
}

void MainWindow::setupTrayIcon()
{
    icon->setToolTip(tr("Wayad"));
    icon->show();
}

void MainWindow::on_actionLogSomething_triggered()
{
    askLog(false);
}

void MainWindow::on_actionConfigureAPI_triggered()
{
    apiPrefsForm->show();
}
