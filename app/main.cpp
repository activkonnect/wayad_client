#include "mainwindow.h"
#include "singleapp.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QApplication::setOrganizationName("ActivKonnect");
    QApplication::setOrganizationDomain("aksrv.net");
    QApplication::setApplicationName("Wayad");
    QApplication::setQuitOnLastWindowClosed(false);

    SingleApp singleApp("b090c7de-7aef-43bb-a72a-24a7043ca224");

    if (singleApp.isOpen()) {
        return 0;
    }

    MainWindow w;
    w.show();

    QObject::connect(&singleApp, SIGNAL(anotherStart()), &w, SLOT(getBack()));

    return a.exec();
}
