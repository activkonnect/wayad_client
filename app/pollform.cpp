#include "pollform.h"
#include "ui_pollform.h"

#include <QtDebug>
#include <QKeyEvent>
#include <QDesktopWidget>
#include <QStandardItemModel>
#include <QDirModel>
#include <QComboBox>
#include <QCompleter>
#include <QAbstractItemView>
#include <QLineEdit>

PollForm::PollForm(QStandardItemModel *references, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PollForm),
    shortcutNothing(new QShortcut(QKeySequence(tr("Esc", "Log Entry|Do Nothing")), this)),
    shortcutAdd(new QShortcut(QKeySequence(tr("Enter", "Log Entry|Do Add")), this)),
    shortcutAnother(new QShortcut(QKeySequence(tr("Ctrl+Enter", "Log Entry|Do Another")), this)),
    openDate(QDateTime::currentDateTime()),
    references(references),
    selectedId(0)
{
    ui->setupUi(this);

    connect(shortcutNothing, SIGNAL(activated()), this, SLOT(doNothing()));
    connect(shortcutAdd, SIGNAL(activated()), this, SLOT(doAdd()));
    connect(shortcutAnother, SIGNAL(activated()), this, SLOT(doAnother()));

    ui->lastAction->installEventFilter(this);
    ui->lastAction->setModel(references);

    ui->btnNothing->setText(ui->btnNothing->text() + " (" + shortcutNothing->key().toString() + ")");
    ui->btnAdd->setText(ui->btnAdd->text() + " (" + shortcutAdd->key().toString() + ")");
    ui->btnAnother->setText(ui->btnAnother->text() + " (" + shortcutAnother->key().toString() + ")");

    ui->openStart->setText(openDate.toString(ui->openStart->text()));

    connect(ui->lastAction, SIGNAL(activated(int)), this, SLOT(activated(int)));

    center();
}

PollForm::~PollForm()
{
    delete ui;
}

bool PollForm::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = (QKeyEvent *) event;

        if (keyEvent->key() == Qt::Key_Return
                && ui->lastAction->currentIndex() > 0
                && !ui->lastAction->completer()->popup()
                && !ui->lastAction->lineEdit()->hasSelectedText()) {
            if (keyEvent->modifiers() == Qt::ControlModifier) {
                doAnother();
            } else {
                doAdd();
            }
        }
    }

    return QWidget::eventFilter(object, event);
}

void PollForm::on_btnNothing_clicked()
{
    doNothing();
}

void PollForm::on_btnAdd_clicked()
{
    doAdd();
}

void PollForm::doNothing()
{
    this->deleteLater();
    this->close();
}

void PollForm::doAdd()
{
    if (ui->lastAction->currentIndex() >= 0) {
        QVariant data = ui->lastAction->currentData(Qt::UserRole + 1);

        if (!data.isNull()) {
            emit logEnteredReference(data.toInt(), openDate);
        } else {
            emit logEnteredMessage(ui->lastAction->currentText(), openDate);
        }

        deleteLater();
        close();
    }
}

void PollForm::doAnother()
{
    if (ui->lastAction->currentIndex() >= 0) {
        doAdd();
        emit needAnother();
    }
}

void PollForm::center()
{
    QRect desktopRect = QApplication::desktop()->availableGeometry();
    QPoint center = desktopRect.center();

    move(center.x() - width() / 2, center.y() - height() / 2);
}

void PollForm::activated(int index)
{
    if (index >= 0) {
        ui->btnAdd->setEnabled(true);
        ui->btnAnother->setEnabled(true);
    } else {
        ui->btnAdd->setEnabled(false);
        ui->btnAnother->setEnabled(false);
    }
}

void PollForm::on_btnAnother_clicked()
{
    doAnother();
}

void PollForm::closeEvent(QCloseEvent *)
{
    deleteLater();
}
