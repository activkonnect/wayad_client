#ifndef REFERENCESMODEL_H
#define REFERENCESMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QJsonArray>
#include <QVector>

class ReferencesModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit ReferencesModel(QObject *parent = 0);
    ~ReferencesModel();

    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int, Qt::Orientation, int) const;

public slots:
    void update(QJsonArray data);

private:
    QVector<int> ids;
    QVector<QString> messages;
    QVector<QString> extras;
};

#endif // REFERENCESMODEL_H
