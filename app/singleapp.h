#ifndef SINGLEAPP_H
#define SINGLEAPP_H

#include <QObject>
#include <QSharedMemory>
#include <QApplication>
#include <QUdpSocket>

class SingleApp : public QObject
{
    Q_OBJECT
public:
    const static int KEY_PORT = 23854;

    explicit SingleApp(QString appKey, QObject *parent = 0);
    ~SingleApp();

    bool isOpen();
    QString memoryKey();

signals:
    void anotherStart();

private:
    QString appKey;
    QUdpSocket socket;

private slots:
    void gotDatagram();
};

#endif // SINGLEAPP_H
